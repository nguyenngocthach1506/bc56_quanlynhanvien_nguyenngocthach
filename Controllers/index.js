/* */
/**********************************INDEX.JS****************************************/
/* */


/*  Thêm mới nhân viên */
document.querySelector('#btnThemNV').onclick = function () {
    /* ------------------------------------------------------------*/
    var taiKhoan = document.getElementById('tknv').value;
    var hoTen = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var matKhau = document.getElementById('password').value;
    var ngayLam = document.getElementById('datepicker').value;
    var luongCanBan = document.getElementById('luongCB').value;
    var chucVu = document.getElementById('chucvu');
    var gioLam = document.getElementById('gioLam').value;
    /* ------------------------------------------------------------*/

    //  Tao nhân viên
    var nhanvien = CreateNhanVien(
        taiKhoan,
        hoTen, email,
        matKhau, ngayLam,
        luongCanBan, chucVu.value,
        chucVu.options.selectedIndex, gioLam);
    //  Thêm nhân viên
    if (nhanvien != null) {
        AddNhanVien(nhanvien);
    }
    else
        alert("Thêm KHÔNG thành công");
}

/*  Cập nhật lại nhân viên */
document.querySelector('#btnCapNhat').onclick = function () {
    /* ------------------------------------------------------------*/
    var taiKhoan = document.getElementById('tknv').value;
    var hoTen = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var matKhau = document.getElementById('password').value;
    var ngayLam = document.getElementById('datepicker').value;
    var luongCanBan = document.getElementById('luongCB').value;
    var chucVu = document.getElementById('chucvu');
    var gioLam = document.getElementById('gioLam').value;
    /* ------------------------------------------------------------*/

    //  Tao nhân viên
    var nhanvien = CreateNhanVien(
        taiKhoan,
        hoTen, email,
        matKhau, ngayLam,
        luongCanBan, chucVu.value,
        chucVu.options.selectedIndex, gioLam);
    //  Cập nhật lại nhân viên
    if (nhanvien != null) {
        UpdateNhanVien(nhanvien);
    }
    else
        alert("Cập nhật KHÔNG thành công");
}

/*  Lọc nhân viên theo XepLoai */
document.querySelector('#searchName').oninput = function (event) {
    var timXepLoai = event.target.value;
    var search = SearchNhanVien(null, null, timXepLoai);
    RenderTableNhanVien(search);
}

/*  Load Danh Sach Nhan Vien tu LocalStorage */
document.body.onload = function () {
    var list;
    try {
        if (GetLocalStorageJSON("DanhSach_NV")) {
            list = GetLocalStorageJSON("DanhSach_NV");
        }
        else
            throw new Error("Lỗi GetLocalStorageJSON()");
    }
    catch (err) {
        console.error(err);
    }

    if (list != null) {
        for (const object of list) {
            var nhanvien = CreateNhanVien(
                object._taikhoan,
                object._hoten, object._email,
                object._matkhau, object._ngaylam,
                object._luongcanban, object._tenchucvu,
                object._index, object._giolam);

            if (nhanvien != null) {
                nhanvien._id = object._id;
                DanhSach_NV.push(nhanvien);
            }
        }
        RenderTableNhanVien(DanhSach_NV);
    }
}
