/* ======================= DOM =================================== */
const TableDanhSach = document.querySelector("#tableDanhSach");
const tbTKNV = document.querySelector('#tbTKNV');
const tbTen = document.querySelector('#tbTen');
const tbEmail = document.querySelector('#tbEmail');
const tbMatKhau = document.querySelector('#tbMatKhau');
const tbNgay = document.querySelector('#tbNgay');
const tbLuongCB = document.querySelector('#tbLuongCB');
const tbChucVu = document.querySelector('#tbChucVu');
const tbGiolam = document.querySelector('#tbGiolam');

/* ========================Khai báo Global======================== */
var DanhSach_NV = []; // Danh sách nhân viên
var valid_NV = true; // Kiểm tra hợp lệ: true| không hợp lệ: false
var index_NV = -1;  // Vị trí nhân viên được tìm thấy
/* =============================================================== */

var checkTimeOut = true;

/*  Khởi tạo Object nhân viên 
    Output: Object Nhân viên */
function CreateNhanVien(taiKhoan,
    hoTen, email, matKhau, ngayLam, luongCanBan, tenchucVu, index, gioLam) {

    /* CLear Thônng Báo */
    ClearThongBao();
    /*--------------------------- */

    // Kiểm tra trống
    valid_NV = EmptyValid(tbTKNV, taiKhoan)
        & EmptyValid(tbTen, hoTen)
        & EmptyValid(tbEmail, email)
        & EmptyValid(tbMatKhau, matKhau)
        & EmptyValid(tbNgay, ngayLam)
        & EmptyValid(tbLuongCB, luongCanBan.toString())
        & EmptyValid(tbGiolam, gioLam.toString());
    // Kiểm tra tài khoản số từ 4 - 6
    valid_NV &= KiemTraKySoValid(tbTKNV, taiKhoan);
    // Kiểm tra họ tên là chữ
    valid_NV &= KiemTraHoTenValid(tbTen, hoTen);
    // Kiểm tra Email
    valid_NV &= KiemTraEmailValid(tbEmail, email);
    // Kiêm tra mật khẩu
    valid_NV &= KiemTraMatKhauValid(tbMatKhau, matKhau);
    // Kiểm Ngày Làm mm/dd/yyyy
    valid_NV &= KiemTraNgayLamValid(tbNgay, ngayLam);
    // Kiểm tra lương căn bản từ 1000000 - 20000000
    valid_NV &= KiemTraNaN(tbLuongCB, luongCanBan) & KiemTraDoDaiValid(tbLuongCB, luongCanBan, 1000000, 20000000);
    // Kiểm tra chức vụ (Giám đốc, Trưởng Phòng, Nhân Viên)
    valid_NV &= KiemTraChucVuValid(tbChucVu, index);
    // Kiểm tra số giờ làm 80-200
    valid_NV &= KiemTraNaN(tbGiolam, gioLam) & KiemTraDoDaiValid(tbGiolam, gioLam, 80, 200)

    if (!valid_NV) {
        // checkTimeOut : khi đang clear thông báo => true 
        // checkTimeOut : khi clear thông báo xong => false 
        if (checkTimeOut) {
            checkTimeOut = false;
            setTimeout(function () { ClearThongBao(); checkTimeOut = true; }, 10000);
        }
        return null;
    }
    else {
        var nhanvien = new NhanVien();
        nhanvien._taikhoan = taiKhoan;
        nhanvien._hoten = hoTen;
        nhanvien._email = email;
        nhanvien._matkhau = matKhau;
        nhanvien._ngaylam = ngayLam;
        nhanvien._luongcanban = luongCanBan * 1;
        nhanvien._tenchucvu = tenchucVu;
        nhanvien._index = index;
        nhanvien._giolam = gioLam * 1;
        return nhanvien;
    }
}

/*  Xóa thông báo Validations */
function ClearThongBao() {
    tbTKNV.innerHTML = "";
    tbTen.innerHTML = "";
    tbEmail.innerHTML = "";
    tbMatKhau.innerHTML = "";
    tbNgay.innerHTML = "";
    tbLuongCB.innerHTML = "";
    tbGiolam.innerHTML = "";
    tbChucVu.innerHTML = "";
}

/*  Add object nhân viên vào danh sách */
function AddNhanVien(nhanvien) {
    try {
        if (nhanvien != null) {
            DanhSach_NV.push(nhanvien);
            RenderTableNhanVien(DanhSach_NV);
            CreateLocalStorageJSON(DanhSach_NV);
            alert("Thêm thành công");
        }
        else
            throw new Error("Lỗi AddNhanVien()");
    }
    catch (err) {
        console.error(err);
    }
}

/*  Xóa Nhân viên theo id */
function DeleteNhanVien(event, id) {
    TimViTriNhanVien(id);
    var index = event.target.getAttribute("data-index") * 1;

    if (index_NV != -1 && index == index_NV) {
        DanhSach_NV.splice(index_NV, 1);   // Xóa 
        CreateLocalStorageJSON(DanhSach_NV);
        RenderTableNhanVien(DanhSach_NV);
    }
    else {
        //  Xóa không thành công
        alert("Xóa không thành công");
    }
}

/*  Tìm nhân viên theo id
    Output: vị trí tìm thấy = index trong Array, Nếu không tìm thấy return -1 */
function TimViTriNhanVien(id) {
    for (const key in DanhSach_NV) {
        if (DanhSach_NV[key]._id == id) {
            index_NV = key;
            return;
        }
        else
            index_NV = -1;
    }
}

/*  Chọn nhật Nhân viên theo id */
function ChooseNhanVien(event, id) {
    TimViTriNhanVien(id);
    var index = event.target.getAttribute("data-index");
    try {
        if (index_NV != -1 && index == index_NV) {
            document.getElementById('tknv').value = DanhSach_NV[index]._taikhoan;
            document.getElementById('name').value = DanhSach_NV[index]._hoten;
            document.getElementById('email').value = DanhSach_NV[index]._email;
            document.getElementById('password').value = DanhSach_NV[index]._matkhau;
            document.getElementById('datepicker').value = DanhSach_NV[index]._ngaylam;
            document.getElementById('luongCB').value = DanhSach_NV[index]._luongcanban;
            document.getElementById('chucvu').options.selectedIndex = DanhSach_NV[index]._index;
            document.getElementById('gioLam').value = DanhSach_NV[index]._giolam;
        }
        else
            //  Không tìm thấy
            throw new Error("Lỗi ChooseNhanVien()")
    }
    catch (err) {
        console.error(err);
    }
}

/*  Cập nhật Nhân viên theo index_NV */
function UpdateNhanVien(nhanvien) {
    try {
        if (index_NV != -1 && nhanvien != null) {
            DanhSach_NV[index_NV]._taikhoan = nhanvien._taikhoan;
            DanhSach_NV[index_NV]._hoten = nhanvien._hoten;
            DanhSach_NV[index_NV]._email = nhanvien._email;
            DanhSach_NV[index_NV]._matkhau = nhanvien._matkhau;
            DanhSach_NV[index_NV]._ngaylam = nhanvien._ngaylam;
            DanhSach_NV[index_NV]._luongcanban = nhanvien._luongcanban;
            DanhSach_NV[index_NV]._tenchucvu = nhanvien._tenchucvu;
            DanhSach_NV[index_NV]._index = nhanvien._index;
            DanhSach_NV[index_NV]._giolam = nhanvien._giolam;

            RenderTableNhanVien(DanhSach_NV);
            CreateLocalStorageJSON(DanhSach_NV);
            alert("Cập nhật thành công");
        }
        else
            throw new Error("Lỗi UpdateNhanVien()");
    }
    catch (err) {
        console.error(err);
    }
}

/*  Chuyển các ký tự đặc biệt, có dấu, khoảng trống
    Ouput : 1 string - vd:ab-c1-deq */
function stringToSlug(title) {
    //Đổi chữ hoa thành chữ thường
    slug = title.toLowerCase();

    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, "-");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');

    return slug;
}

/*  Search Nhân viên
    Output: Danh sách nhân viên */
function SearchNhanVien(id, name, xeploai) {
    var danhSachSearch = [];
    if (xeploai != "" || xeploai != null) {
        xeploai = stringToSlug(xeploai);
        for (let index = 0; index < DanhSach_NV.length; index++) {
            var x = DanhSach_NV[index].XepLoai();
            x = stringToSlug(x);
            if (x.search(xeploai) != -1)
                danhSachSearch.push(DanhSach_NV[index]);
        }
        return danhSachSearch;
    }
    else
        RenderTableNhanVien(DanhSach_NV);
}
/*  Show Danh Sách nhân viên vào #tableDanhSach */
function RenderTableNhanVien(list = []) {
    var tr = "";

    for (let index = 0; index < list.length; index++) {
        const NHANVIEN = list[index];
        tr += `<tr>
        <td>${NHANVIEN._taikhoan}</td>
        <td>${NHANVIEN._hoten}</td>
        <td>${NHANVIEN._email}</td>
        <td>${NHANVIEN._ngaylam}</td>
        <td>${NHANVIEN._tenchucvu}</td>
        <td>${NHANVIEN.TongLuong().toLocaleString()}</td>
        <td>${NHANVIEN.XepLoai()}</td>
        <td>
            <button class="btn bg-danger w-100" data-index="${index}" onclick="DeleteNhanVien(event, ${NHANVIEN._id})" type="button">Xóa</button>
            <button class="btn bg-warning w-100" data-index="${index}" onclick="ChooseNhanVien(event, ${NHANVIEN._id})" type="button">Chọn</button>
        </td>
    </tr>`;
    }

    TableDanhSach.innerHTML = tr;
}

/*  Khởi tạo LocaleStorage */
function CreateLocalStorageJSON(list) {
    // Chuyển Array về chuỗi
    var string = JSON.stringify(list);
    // Lưu chuỗi vào storage
    localStorage.setItem("DanhSach_NV", string);
}

/*  Lấy LocaleStorage
    Output: Array các object từ LocalsStorage */
function GetLocalStorageJSON(localName) {
    if (localStorage.getItem(localName)) {
        var string = localStorage.getItem(localName);
        var valueJSON = JSON.parse(string);
        return valueJSON;
    }
    return null;
}



