function NhanVien() {
    this._id = new Date().getTime().toString();
    this._taikhoan = "";
    this._hoten = "";
    this._email = "";
    this._matkhau = "";
    this._ngaylam = "";
    this._luongcanban = 0;
    this._giolam = 0;

    this.ChucVu = function ChucVu() {
        this._index = 0;
        this._tenchucvu = "";
        this._heso = 1;
    }

    this.TongLuong = function () {
        if (this._tenchucvu.search("Sếp") != -1) {
            this._heso = 3;
        }
        if (this._tenchucvu.search("Trưởng phòng") != -1) {
            this._heso = 2;
        }
        if (this._tenchucvu.search("Nhân viên") != -1) {
            this._heso = 1;
        }

        return this._luongcanban * this._heso;
    }

    this.XepLoai = function () {
        const GIOLAM = this._giolam;
        if (GIOLAM >= 192) {
            return "Nhân viên Xuất Sắc";
        }
        else
            if (GIOLAM >= 176) {
                return "Nhân viên Giỏi";
            }
            else
                if (GIOLAM >= 160) {
                    return "Nhân viên Khá";
                }
                else {
                    return "Nhân viên Trung Bình";
                }
    }
}

